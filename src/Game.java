
import java.util.Scanner;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import java.awt.*;
import java.awt.event.*;

public class Game implements ActionListener,KeyListener {
	private int dim = 4;
	private JFrame frame = new JFrame();
	private JLabel [][] label;
	private JPanel main = new JPanel();
	private JPanel plateau = new JPanel();
	private JPanel menu = new JPanel();
	private JPanel fin = new JPanel();
	private JPanel param = new JPanel();
	private JButton btnRetour;
	private JButton playButton;
	private JButton replay;
	private JButton btnOption;
	private JButton btnMenuPrincipal;
	private Grille g;
	private JLabel score = new JLabel();
	
	private JRadioButton rdbtnNormal;
	private JRadioButton rdbtnFacile;
	private JRadioButton rdbtnDifficile;
	
	private JRadioButton rdbtnSombre;
	private JRadioButton rdbtnClair;
	
	
	private Color fontColor = Color.white;
	private Color bgColor = Color.DARK_GRAY;
	
	private boolean partieEnCours = false;
	
	private CardLayout layout = new CardLayout();
	//Constructeur qui initialise la JFrame et le cardLayout 
	public Game(){
		main.setLayout(layout);
		
		menuPrincipal();
		
		main.add(menu,"Menu");
		main.add(plateau,"Jeu");
		main.add(fin,"Fin");
		main.add(param,"Option");
		

		frame.setPreferredSize(new Dimension(400, 450));
		frame.add(main);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setFocusable(true);
		frame.addKeyListener(this);
		frame.requestFocus();
		frame.setTitle("2048");
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		layout.show(main, "Menu");
	}
	//Fonction GUI du menu principal
	private void menuPrincipal() {
		menu.removeAll();
		menu.revalidate();
		menu.repaint();
		
		JLabel titre = new JLabel("2048");
		titre.setHorizontalAlignment(SwingConstants.CENTER);
		titre.setForeground(fontColor);
		titre.setFont(new Font("SansSerif", Font.BOLD, 60));
		
		JLabel author = new JLabel("Hippolyte Mithouard");
		author.setForeground(fontColor);
		author.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		playButton = new JButton("Jouer");
		playButton.addActionListener(this);
		
		btnOption = new JButton("Paramètres");
		btnOption.addActionListener(this);
		
		GroupLayout groupLayout = new GroupLayout(menu);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(158)
								.addComponent(playButton))
							.addComponent(author, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(118)
								.addComponent(titre))
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(139)
								.addComponent(btnOption)))
						.addContainerGap(122, Short.MAX_VALUE))
			);
			groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
					.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap(142, Short.MAX_VALUE)
						.addComponent(titre)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(playButton)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnOption)
						.addGap(139)
						.addComponent(author, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
		menu.setLayout(groupLayout);
		menu.setBackground(bgColor);
	}
	//Fonction GUI du menu Option
	private void menuOption() {
		param.removeAll();
		param.revalidate();
		param.repaint();
		
		
		ButtonGroup diff = new ButtonGroup();
		ButtonGroup sk = new ButtonGroup();
		rdbtnFacile = new JRadioButton("Facile");
		rdbtnFacile.setForeground(fontColor);
		rdbtnFacile.setBackground(bgColor);
		rdbtnFacile.addActionListener(this);
		
		rdbtnNormal = new JRadioButton("Normal");
		rdbtnNormal.setForeground(fontColor);
		rdbtnNormal.setBackground(bgColor);
		rdbtnNormal.addActionListener(this);
		
		rdbtnDifficile = new JRadioButton("Difficile");
		rdbtnDifficile.setForeground(fontColor);
		rdbtnDifficile.setBackground(bgColor);
		rdbtnDifficile.addActionListener(this);
		
		rdbtnSombre = new JRadioButton("Sombre");
		rdbtnSombre.setForeground(fontColor);
		rdbtnSombre.setBackground(bgColor);
		rdbtnSombre.addActionListener(this);
		
		rdbtnClair = new JRadioButton("Clair");
		rdbtnClair.setForeground(fontColor);
		rdbtnClair.setBackground(bgColor);
		rdbtnClair.addActionListener(this);
		
		btnRetour = new JButton("Retour");
		btnRetour.addActionListener(this);
		
		diff.add(rdbtnFacile);
		diff.add(rdbtnNormal);
		diff.add(rdbtnDifficile);
		
		sk.add(rdbtnSombre);
		sk.add(rdbtnClair);
		
		JLabel option = new JLabel("Paramètres");
		option.setFont(new Font("SansSerif", Font.BOLD, 30));
		option.setForeground(fontColor);
		
		JLabel Difficulty = new JLabel("Difficulté");
		Difficulty.setFont(new Font("SansSerif", Font.BOLD, 13));
		Difficulty.setForeground(fontColor);
		
		JLabel skin = new JLabel("Apparence");
		skin.setFont(new Font("SansSerif", Font.BOLD, 13));
		skin.setForeground(fontColor);
		
		GroupLayout groupLayout = new GroupLayout(param);
		
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(72)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(rdbtnDifficile, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
									.addGroup(groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(rdbtnNormal, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
											.addComponent(rdbtnFacile)
											.addComponent(Difficulty))
										.addGap(30)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(rdbtnSombre, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
											.addComponent(rdbtnClair, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
											.addComponent(skin)))))
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(102)
								.addComponent(option))
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(152)
								.addComponent(btnRetour)))
						.addContainerGap(88, Short.MAX_VALUE))
			);
			groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(groupLayout.createSequentialGroup()
						.addGap(30)
						.addComponent(option)
						.addGap(26)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(skin)
							.addComponent(Difficulty))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(rdbtnSombre, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
							.addComponent(rdbtnFacile, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(rdbtnClair, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
							.addComponent(rdbtnNormal, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(rdbtnDifficile, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 212, Short.MAX_VALUE)
						.addComponent(btnRetour)
						.addGap(23))
			);
		param.setLayout(groupLayout);
		param.setBackground(bgColor);
		
		buttonSelected();
	}
	//Fonction GUI de l'écran de fin
	public void ecranDeFin() {
		fin.removeAll();
		fin.revalidate();
		fin.repaint();
		
		JLabel perdu = new JLabel("Perdu !");
		perdu.setAlignmentX(Component.CENTER_ALIGNMENT);
		perdu.setHorizontalAlignment(SwingConstants.CENTER);
		perdu.setForeground(fontColor);
		perdu.setFont(new Font("SansSerif", Font.BOLD, 30));
		
		score.setText("Score : "+g.getScore());
		score.setFont(new Font("SansSerif", Font.BOLD, 13));
		score.setForeground(fontColor);
		
		replay = new JButton("Rejouer ?");
		replay.addActionListener(this);
		
		btnMenuPrincipal = new JButton("Menu Principal");
		btnMenuPrincipal.addActionListener(this);
		
		GroupLayout groupLayout = new GroupLayout(fin);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(groupLayout.createSequentialGroup()
						.addGap(141)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(replay)
							.addComponent(perdu))
						.addContainerGap(150, Short.MAX_VALUE))
					.addGroup(groupLayout.createSequentialGroup()
						.addGap(123)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(6)
								.addComponent(score, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE))
							.addComponent(btnMenuPrincipal))
						.addContainerGap(120, Short.MAX_VALUE))
			);
			groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
					.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap(101, Short.MAX_VALUE)
						.addComponent(perdu)
						.addGap(18)
						.addComponent(replay)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnMenuPrincipal)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(score, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGap(197))
			);
		fin.setLayout(groupLayout);
		fin.setBackground(bgColor);
	}
	//Met les raddioButtons en état Selected pars rapport à l'état du jeu
	private void buttonSelected() {
		Color c = Color.DARK_GRAY;
		switch(dim) {
		case(5):
			rdbtnFacile.setSelected(true);
			break;
		case(4):
			rdbtnNormal.setSelected(true);
			break;
		case(3):
			rdbtnDifficile.setSelected(true);
			break;
		}
		if(bgColor == c)rdbtnSombre.setSelected(true);
		else rdbtnClair.setSelected(true);
	}
	//Fonction d'update de la fenêtre de jeu
	private void update() {
		int i,j,value;
		Color col;
		label = new JLabel [dim][dim];
		if(g.isFin()) {
			ecranDeFin();
			layout.show(main, "Fin");
		}
		else {
			plateau.removeAll();
			plateau.revalidate();
			plateau.repaint();
			for(i=0;i<dim;i++) {
				for(j=0;j<dim;j++) {
					value = g.getValue(i,j);
					col = new Color(255000000-(value*100));
					if(value!=0) {
						label[i][j] = new JLabel(""+value,SwingConstants.CENTER);
						label[i][j].setForeground(Color.white);
						label[i][j].setOpaque(true);
						label[i][j].setBackground(col);
						label[i][j].setBorder(new LineBorder(Color.DARK_GRAY));
						label[i][j].setPreferredSize(new Dimension(100, 100));
						plateau.add(label[i][j]);
					}else {
						label[i][j] = new JLabel(" ",SwingConstants.CENTER);
						label[i][j].setOpaque(true);
						label[i][j].setBackground(Color.LIGHT_GRAY);
						label[i][j].setBorder(new LineBorder(bgColor));
						label[i][j].setPreferredSize(new Dimension(100, 100));
						plateau.add(label[i][j]);
					}
				}
			}
			score.setText("Score : "+g.getScore());
			score.setForeground(fontColor);
			plateau.add(score);
		}
	}
	// Methode pour version sur terminal
	public void partie() {
		boolean partie = true;
		char input;
		this.g.afficheGrille();
		try(Scanner scanner = new Scanner(System.in)){
			while(partie) {
				System.out.println("Saisir un déplacement (a : liste déplacement) : \n");
				input = scanner.next().charAt(0);
				switch(input) {
				case('a'):
					System.out.println("d : Droite\ng: Gauche\nh : Haut\nb : Bas\ne : Finir la partie\n");
					break;
				case('e'):
					partie = false;
					break;
				default:
					g.deplacement(input);
					break;
				}
				g.afficheGrille();
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Game();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == playButton) {
			g = new Grille(dim);
			plateau.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			plateau.setLayout(new GridLayout(dim+1,dim));
			plateau.setBackground(bgColor);
			layout.show(main, "Jeu");
			partieEnCours = true;
			update();
		}
		if(e.getSource() == replay) {
			g = new Grille(dim);
			layout.show(main,"Jeu");
			update();
		}
		if(e.getSource() == rdbtnSombre) {
			fontColor = Color.white;
			bgColor = Color.DARK_GRAY;
			menuOption();
		}
		if(e.getSource() == rdbtnClair) {
			fontColor = Color.black;
			bgColor = Color.white;
			menuOption();
		}
		if(e.getSource() == rdbtnFacile) {
			dim = 5;
		}
		if(e.getSource() == rdbtnNormal) {
			dim = 4;
		}
		if(e.getSource() == rdbtnDifficile) {
			dim = 3;
		}
		if(e.getSource() == btnRetour) {
			menuPrincipal();
			layout.show(main, "Menu");
		}
		if(e.getSource() == btnOption) {
			menuOption();
			layout.show(main, "Option");
		}
		if(e.getSource() == btnMenuPrincipal) {
			layout.show(main, "Menu");
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int keycode = e.getKeyCode();
		if(partieEnCours) {
			switch(keycode) {
			case(KeyEvent.VK_UP):
				g.deplacement('h');
				update();
				break;
			case(KeyEvent.VK_DOWN):
				g.deplacement('b');
				update();
				break;
			case(KeyEvent.VK_LEFT):
				g.deplacement('g');
				update();
				break;
			case(KeyEvent.VK_RIGHT):
				g.deplacement('d');
				update();
				break;
			
			}
		}
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
