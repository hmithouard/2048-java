
public class Grille {
	private int [][] tab ;
	private int [] pieces = {2,4};
	private int score;
	private int dim;
	private boolean fin = false;
	public Grille(int i) {
		this.dim = i;
		this.tab = new int [dim] [dim];
		this.score = 0;
		this.initGrille();
		System.out.println(dim);
	}
	//Initialise la grille de jeu avec deux élément aléatoire (soit 2 soit 4)
	//et les placent dans des coords aléatoires.
	private void initGrille() {
		int i,randX,randY;
		for(i = 0 ; i<2; i++) {
			randX = (int)(Math.random() * dim);
			randY = (int)(Math.random() * dim);
			if(this.tab[randX][randY] != 0) i--;
			else this.tab[randX][randY] = this.pieces[(int)(Math.random()*2)];
		}
	}
	//Génére une nouvelle pièce et la place dans la grille de jeu à des coords aléatoires
	private void setNewPiece() {
		int randX,randY;
		while(true) {
			randX = (int)(Math.random() * dim);
			randY = (int)(Math.random() * dim);
			if(this.tab[randX][randY] == 0) {
				this.tab[randX][randY] = this.pieces[(int)(Math.random()*2)];
				break;
			}
		}
		
	}
	//Fonction qui fait la liaison des input de la Classe Game.
	public void deplacement(char c) {
			switch(c) {
			case('b'):
				if(bas())setNewPiece();
				else checkFin();
				break;
			case('h'):
				if(haut())setNewPiece();
				else checkFin();
				break;
			case('g'):
				if(gauche())setNewPiece();
				else checkFin();
				break;
			case('d'):
				if(droite())setNewPiece();
				else checkFin();
				break;
			default:
				System.out.println("Valeur invalide !\n");
				break;
			}
	}
	//Vérifie si la grille est pleine
	public void checkFin() {
		int i,j;
		fin = true;
		for(i = 0; i<getGrilleSize();i++) {
			for(j = 0;j<getGrilleSize();j++) {
				if(getValue(i, j) == 0) fin = false;
			}
		}
		//System.out.println(fin);
	}
	//fonction déplacant tous les éléments vers le bas et fusionne les pièces de même valeurs qui se touchent
	private boolean bas() {
		int i,j,k;
		boolean move = false;
		for (i = this.tab.length-1;i >= 0;i--) {
			for(j = 0 ; j < this.tab.length; j++) {
				if(this.tab[i][j] == 0) {
					for(k = i-1 ; k>= 0  ; k--) {
						if(this.tab[k][j] != 0) {
							this.tab[i][j] = this.tab[k][j];
							this.tab[k][j] = 0;
							move = true;
							break;
						}
					}
				}
				if(i != this.tab.length-1 && this.tab[i][j] == this.tab[i+1][j]&& this.tab[i][j] != 0) {
					this.tab[i+1][j] += this.tab[i][j];
					this.score += this.tab[i+1][j];
					this.tab[i][j] = 0;
					move = true;
					i++;
				}
			}
			
		}
		return move;
	}
	//fonction déplacant tous les éléments vers le haut et fusionne les pièces de même valeurs qui se touchent
	private boolean haut() {
		int i,j,k;
		boolean move = false;
		for (i = 0;i < this.tab.length;i++) {
			for(j = 0 ; j < this.tab.length; j++) {
				if(this.tab[i][j] == 0) {
					for(k = i+1 ; k < this.tab.length  ; k++) {
						if(this.tab[k][j] != 0) {
							this.tab[i][j] = this.tab[k][j];
							this.tab[k][j] = 0;
							move = true;
							break;
						}
					}
				}
				if(i != 0 && this.tab[i][j] == this.tab[i-1][j]&& this.tab[i][j] != 0) {
					this.tab[i-1][j] += this.tab[i][j];
					this.score += this.tab[i-1][j];
					this.tab[i][j] = 0;
					move = true;
					i--;
				}
			}
		}
		return move;
	}
	//fonction déplacant tous les éléments vers la droite et fusionne les pièces de même valeurs qui se touchent
	private boolean droite() {
		int i,j,k;
		boolean move = false;
		for (i = 0;i < this.tab.length;i++) {
			for(j = this.tab.length-1; j >= 0; j--) {
				if(this.tab[i][j] == 0) {
					for(k = j-1 ; k >= 0  ; k--) {
						if(this.tab[i][k] != 0) {
							this.tab[i][j] = this.tab[i][k];
							this.tab[i][k] = 0;
							move = true;
							break;
						}
					}
				}
				if(j != this.tab.length-1 && this.tab[i][j] == this.tab[i][j+1]&& this.tab[i][j] != 0) {
					this.tab[i][j+1] += this.tab[i][j];
					this.score += this.tab[i][j+1];
					this.tab[i][j] = 0;
					move = true;
					j++;
				}
			}
		}
		return move;
	}
	//fonction déplacant tous les éléments vers la gauche et fusionne les pièces de même valeurs qui se touchent
	private boolean gauche() {
		int i,j,k;
		boolean move = false;
		for (j = 0;j < this.tab.length;j++) {
			for(i = 0; i < this.tab.length; i++) {
				if(this.tab[i][j] == 0) {
					for(k = j + 1 ; k < tab.length  ; k++) {
						if(this.tab[i][k] != 0) {
							this.tab[i][j] = this.tab[i][k];
							this.tab[i][k] = 0;
							move = true;
							break;

						}
					}
				}
				if(j != 0 && this.tab[i][j] == this.tab[i][j-1] && this.tab[i][j] != 0){
					this.tab[i][j-1] += this.tab[i][j];
					this.score += this.tab[i][j-1];
					this.tab[i][j] = 0;
					move = true;
					j--;
				}
			}
		}
		return move;
	}
	public int getScore() {
		return this.score;
	}
	public int getGrilleSize() {
		return dim;
	}
	public int getValue(int i, int j) {
		return this.tab[i][j];
	}
	public boolean isFin() {
		return fin;
	}
	public void setFin(boolean fin) {
		this.fin = fin;
	}
	//Affichage de la grille dans le terminal
	public void afficheGrille() {
		int i,j;
		for(i = 0; i < this.tab.length;i++) {
			System.out.println("-----------------\n");
			for(j = 0; j < this.tab.length;j++) {
				switch(j) {
				case(0):
					if (this.tab[i][j] == 0) System.out.printf("|   |",tab[i][j]);
					else System.out.printf("| %d |",tab[i][j]);
					break;
				default:
					if (this.tab[i][j] == 0) System.out.printf("   |",tab[i][j]);
					else System.out.printf(" %d |",tab[i][j]);
					break;
				}
				
			}
			System.out.println("\n");
		}
		System.out.println("-----------------\n");
	}
}
